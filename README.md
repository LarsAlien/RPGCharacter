# RPGCharacters

RPGCharacters is a console application for managing your adventurers. Create, manage and use items for your character.

## Features
* Create new adventurers: Brutes, Bowmasters and Magisters
* List all adventurers in guild
* Equip armor and weapons to increase stats (Items scale with level and different attribute types: HP,DEX,STR,INT)
* Attack someone and see damage output
* Your adventurer and items level up as they attack and attributes scale on level up

## Usage

Clone and run the solution in your IDE of choice (I used Visual Studio)

## Structure

```
RPGCharacters
|
└───Adventurers - Contains all logic for creating new adventurers. Including: Basestats for AdventurerClasses, Random name generator
│   
└───ConsoleInteraction - Contains all logic for Interacting with user. Including: Menus, inputreaders and printingservice
│   │
│   └───InteractionMenus - All user menus for manipulating adventurer and item data
│   |   
|   └───Services - Statprinter and InputReader
│       
└───Enums - Contains all constants for easier manipulation
|
└───Items - Contains all logic for creating new Items. Including: Weapons, Armor
```
## Presentation.pdf

This document demonstrates how to create adventurers and equip them with items. See how attribute scale with levels and item bonuses
