﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Enums
{
    // Contains all constants
    // Makes it more difficult to make random equal check mistakes
    /// <summary>
    /// Enum for all available Attributes
    /// </summary>
    enum Attributes
    {
        HP,
        STR,
        DEX,
        INT
    }
    /// <summary>
    /// Enum for all available ArmorTypes
    /// </summary>
    enum ArmorTypes
    {
        CLOTH,
        LEATHER,
        PLATE
    }
    /// <summary>
    /// Enum for all available WeaponTypes
    /// </summary>
    enum WeaponTypes
    {
        MELEE,
        RANGED,
        MAGIC
    }
    /// <summary>
    /// Enum for all available ItemSlots
    /// </summary>
    enum ItemSlots
    {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }
    /// <summary>
    /// Enum for all available AdventurerClasses
    /// </summary>
    enum AdventurerClasses
    {
        BRUTE,
        BOWMASTER,
        MAGISTER
    }
}
