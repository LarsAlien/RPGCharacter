﻿using RPGCharacters.Enums;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters.Adventurers
{
    class Adventurer
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int ExperienceToNxtLvl { get; set; }

        // Base attributes
        readonly int baseHealth;
        readonly int baseStrength;
        readonly int baseDexterity;
        readonly int baseIntelligence;

        // Base attribute scaling
        readonly int healthScaling;
        readonly int strengthScaling;
        readonly int dexterityScaling;
        readonly int intelligenceScaling;

        // Effective attributes. After scaling and bonuses
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        AdventurerClasses adventurerClass;

        /// <summary>
        /// Use Adventurer.EquipWeapon to equip
        /// </summary>
        public Weapon weapon;

        /// <summary>
        /// Available slots for armor. Use Adventurer.EquipArmor to equip
        /// </summary>
        public IDictionary<ItemSlots, Armor> armor;

        public Adventurer(AdventurerClasses adventurerClass)
        {
            Name = Namery.GetRandomName();
            Level = 1;
            Experience = 0;
            ExperienceToNxtLvl = 100;
            this.adventurerClass = adventurerClass;

            (baseHealth, baseStrength, baseDexterity, baseIntelligence) =
                        AdventurerBaseAttributes.GetBaseAttributes(adventurerClass); // Destructuring base attributes from AdventurerBaseAttributes

            (healthScaling, strengthScaling, dexterityScaling, intelligenceScaling) =
                AdventurerBaseAttributes.GetScalingAttributes(adventurerClass); // Destructuring base scaling from AdventurerBaseAttributes

            UpdateStats();
        }

        /// <summary>
        /// "Attacks someone" and grants experience to adventurer and worn equipment.
        /// </summary>
        public void Attack()
        {
            if (weapon is null)
            {
                Console.WriteLine("You have no weapon! Damage dealt is 0.");
            }
            else
            {
                Console.WriteLine($"Attacked someone and dealt {GetEffectiveDamage()} damage!");
                Console.WriteLine("You gained 100exp!");
                Experience += 100;
                weapon.Use();
                if (armor is {}) {
                    foreach (KeyValuePair<ItemSlots, Armor> item in armor)
                    {
                        item.Value.Use();
                    }
                }
                UpdateStats();

            }
        }

        /// <summary>
        /// Adds ItemSlot and Armor to IDictionary: armor
        /// </summary>
        public void EquipArmor(ItemSlots slot, Armor armor)
        {
            if (this.armor == null) // If adventurer has no armor, initialize Dictionary and add armor in slot
            {
                this.armor = new Dictionary<ItemSlots, Armor>();
                this.armor.Add(slot, armor);
            }
            else if(this.armor.ContainsKey(slot)) // If armor slot is not null, replace Value
            {
                this.armor[slot] = armor;
            }
            else this.armor.Add(slot, armor); // Else add slot and armor
            UpdateStats();
        }

        /// <summary>
        /// Removes KeyValuePair at ItemSlot in IDictionary: armor
        /// </summary>
        public void UnequipArmor(ItemSlots slot)
        {
            if (armor[slot] != null) armor.Remove(slot);
            UpdateStats();
        }
        public void EquipWeapon(Weapon weapon)
        {
            this.weapon = weapon;
        }
        public void UnequipWeapon()
        {
            weapon = null;
        }

        /// <summary>
        /// Calculates effective damage output based on WeaponTypes
        /// </summary>
        public int GetEffectiveDamage()
        {
            if (weapon == null) return 0;
            switch (weapon.WeaponType)
            {
                case WeaponTypes.MELEE:
                    return (int)(weapon.GetDamage() * 1.5 * Strength);
                case WeaponTypes.MAGIC:
                    return (int)(weapon.GetDamage() * 1.3 * Intelligence);
                case WeaponTypes.RANGED:
                    return (int)(weapon.GetDamage() * 1.2 * Dexterity);
                default:
                    return 0;
            }
        }

        public void LevelUp()
        {
            Level++;
            Experience = Experience - ExperienceToNxtLvl;
            ExperienceToNxtLvl = (int)(ExperienceToNxtLvl * 1.1);
            Console.WriteLine($"Congratulations, you Leveled up to Level {Level}! EXP to next Level: {ExperienceToNxtLvl}");
        }

        public void TakeDamage(int damage)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recalculates stats, based on scaling for adventurerClass, Level and tries to apply bonus from equipment
        /// </summary>
        void UpdateStats()
        {
            if (Experience >= ExperienceToNxtLvl)
            {
                LevelUp();
            }

            Health = baseHealth + (Level-1) * healthScaling;
            Strength = baseStrength + (Level - 1) * strengthScaling;
            Dexterity = baseDexterity + (Level - 1) * dexterityScaling;
            Intelligence = baseIntelligence + (Level - 1) * intelligenceScaling;
            try
            {
                ApplyArmorStats();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Applies bonus from equipped armor
        /// </summary>
        void ApplyArmorStats()
        {
            if (armor == null) return;
            foreach (KeyValuePair<ItemSlots, Armor> armor in armor)
            {
                switch (armor.Value.ArmorType)
                {
                    case ArmorTypes.CLOTH:
                        Health += armor.Value.GetBonuses()[Attributes.HP];
                        Intelligence += armor.Value.GetBonuses()[Attributes.INT];
                        Dexterity += armor.Value.GetBonuses()[Attributes.DEX];
                        break;
                    case ArmorTypes.LEATHER:
                    case ArmorTypes.PLATE:
                        Health += armor.Value.GetBonuses()[Attributes.HP];
                        Strength += armor.Value.GetBonuses()[Attributes.STR];
                        Dexterity += armor.Value.GetBonuses()[Attributes.DEX];
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
