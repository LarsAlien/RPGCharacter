﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Adventurers
{
    /// <summary>
    /// Generates random name for your adventurer
    /// </summary>
    static class Namery
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns>Random adventurer name</returns>
        public static string GetRandomName()
        {
            return GetRandomTitle() + GetRandomFirstName() + GetRandomLastName();
        }

        private static string GetRandomLastName()
        {
            Random randomIndex = new Random();
            return lastNames[randomIndex.Next(lastNames.Length)];
        }

        private static string GetRandomFirstName()
        {
            Random randomIndex = new Random();
            return firstNames[randomIndex.Next(firstNames.Length)];
        }

        private static string GetRandomTitle()
        {
            Random randomIndex = new Random();
            return titles[randomIndex.Next(titles.Length)];
        }
        static string[] titles =
        {
            "Lord ",
            "King ",
            "Duke ",
            "Earl ",
            "Bishop ",
            "Count ",
            "Sir ",
            "Alderman ",
            "Admiral ",
            "Vicar ",
            "Monk ",
            "Archbishop ",
            "Prince ",
            "Cardinal ",
            "Chancellor ",
        };

        static string[] firstNames =
        {
            "Merek",
            "Carac",
            "Ulric",
            "Tybalt",
            "Borin",
            "Sadon",
            "Terrowin",
            "Rowan",
            "Forthwind",
            "Althalos",
            "Fendrel",
            "Brom",
            "Hadrian",
            "Lord Crewe",
            "Walter De Bolbec",
            "Earl",
            "Montagu",
            "John Fenwick",
            "Oliver Cromwell",
            "Justice McKinnon",
            "Clifton Writingham",
            "Walter deGrey",
            "Roger de Mowbray",
            "Joseph Rowntree",
            "Geoffrey Chaucer",
            "William",
            "Francis Drake",
            "Simon de Montfort",
            "John",
            "William",
            "Lord Cornwallis",
            "Edmund Cartwright",
            "Charles",
            "Benedict",
            "Gregory",
            "Peter",
            "Henry",
            "Frederick",
            "Walter",
            "Thomas",
            "Arthur",
            "Bryce",
            "Donald",
            "Leofrick",
            "Letholdus",
            "Lief",
            "Barda",
            "Rulf",
            "Robin",
            "Gavin",
            "Terrin/Terryn",
            "Ronald",
            "Jarin",
            "Cassius",
            "Leo",
            "Cedric",
            "Gavin",
            "Peyton",
            "Josef",
            "Janshai",
            "Doran",
            "Asher",
            "Quinn",
            "Zane",
            "Xalvador",
            "Favian",
            "Destrian",
            "Dain",
            "Lord Falk",
            "Berinon",
            "Tristan",
            "Gorvenal",
        };

        static string[] lastNames =
        {
            " the black",
            " the white",
            " the blue",
            " the cyan",
            " the indigo",
            " of The Battle Of Marson Moor",
            " of The Hundred Years War",
            " of The Peasant's Revolt",
            " of The War of the League of Augsburg",
            " of The First Bishop's War",
            " of The Battle of Hastings",
            " of The Wars of the Roses",
            " of The Norman Conquest",
            " of The Pentland Rising",
            " of Kett's Rebellion",
            " of The Battle of Formigny"

        };
    }
}
