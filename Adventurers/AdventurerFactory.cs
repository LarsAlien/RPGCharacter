﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Adventurers
{
    /// <summary>
    /// Contains list of all adventurers and methods to add and get them
    /// </summary>
    static class AdventurerFactory
    {
        public static List<Adventurer> adventurers = new List<Adventurer>();
        public static List<Adventurer> GetAdventurers()
        {
            return adventurers;
        }

        public static void AddAdventurer(Adventurer adventurer)
        {
            adventurers.Add(adventurer);
        }
    }
}
