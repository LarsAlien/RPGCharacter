﻿using RPGCharacters.Enums;

namespace RPGCharacters.Adventurers
{
    /// <summary>
    /// Contains all base attributes for the different AdventurerClasses
    /// </summary>
    class AdventurerBaseAttributes
    {
        int baseHealth;
        int baseStrength;
        int baseDexterity;
        int baseIntelligence;

        public AdventurerBaseAttributes(
            int baseHealth,
            int baseStrength,
            int baseDexterity,
            int baseIntelligence
            )
        {
            this.baseHealth = baseHealth;
            this.baseStrength = baseStrength;
            this.baseDexterity = baseDexterity;
            this.baseIntelligence = baseIntelligence;
        }

        // Deconstructor, helps assign values to variables in other classes
        public void Deconstruct(
            out int baseHealth,
            out int baseStrength,
            out int baseDexterity,
            out int baseIntelligence
            )
        {
            baseHealth = this.baseHealth;
            baseStrength = this.baseStrength;
            baseDexterity = this.baseDexterity;
            baseIntelligence = this.baseIntelligence;
        }

        public static AdventurerBaseAttributes GetBaseAttributes(AdventurerClasses adventurerClass)
        {
            switch (adventurerClass)
            {
                // Base attributes (HP, STRENGTH, DEXTERITY, INTELLIGENCE)
                case AdventurerClasses.BRUTE:
                    return new AdventurerBaseAttributes(150, 10, 3, 1);
                case AdventurerClasses.BOWMASTER:
                    return new AdventurerBaseAttributes(120, 5, 10, 2);
                case AdventurerClasses.MAGISTER:
                    return new AdventurerBaseAttributes(100, 2, 3, 10);
                default:
                    return new AdventurerBaseAttributes(0, 0, 0, 0);
            }
        }

        public static AdventurerBaseAttributes GetScalingAttributes(AdventurerClasses adventurerClass)
        {
            switch (adventurerClass)
            {
                // Attribute scaling (HP, STRENGTH, DEXTERITY, INTELLIGENCE)
                case AdventurerClasses.BRUTE:
                    return new AdventurerBaseAttributes(30, 5, 2, 1);
                case AdventurerClasses.BOWMASTER:
                    return new AdventurerBaseAttributes(20, 2, 5, 1);
                case AdventurerClasses.MAGISTER:
                    return new AdventurerBaseAttributes(15, 1, 2, 5);
                default:
                    return new AdventurerBaseAttributes(0, 0, 0, 0);
            }
        }
    }
}
