﻿using System;
using System.Collections.Generic;
using System.Text;
using RPGCharacters.Enums;

namespace RPGCharacters.Items
{
    class Item
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int ExperienceToNxtLvl { get; set; }
        public static ItemSlots ItemSlot { get; set; }

        public Item():base()
        {
            Level = 1;
            Experience = 0;
            ExperienceToNxtLvl = 80;
        }

        void LevelUp()
        {
            Level++;
            Experience = Experience - ExperienceToNxtLvl;
            ExperienceToNxtLvl = (int)(ExperienceToNxtLvl * 1.1);
            Console.WriteLine($"Congratulations, {Name} Leveled up to Level {Level}! EXP to next Level: {ExperienceToNxtLvl}");
        }

        /// <summary>
        /// Grants experience to item upon use
        /// </summary>
        public void Use()
        {
            Experience += 50;
            if (Experience >= ExperienceToNxtLvl)
            {
                LevelUp();
            }
        }
    }
}
