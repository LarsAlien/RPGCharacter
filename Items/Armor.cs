﻿using System;
using System.Collections.Generic;
using RPGCharacters.Enums;

namespace RPGCharacters.Items
{
    class Armor : Item
    {
        /// <summary>Type of armor(Enums.ArmorTypes)</summary>
        public ArmorTypes ArmorType { get; set; }
        /// <summary>Dictionary with attributes and its initial bonus</summary>
        public IDictionary<Attributes, int> Bonus { get; set; }
        /// <summary>Dictionary with attributes and its initial bonus scaling </summary>
        public IDictionary<Attributes, int> BonusScaling { get; set; }

        public Armor() : base()
        {
            ExperienceToNxtLvl = new Random().Next(80, 200);
        }

        /// <summary>Takes attributes and scales them up based on base bonus, bonus scaling and level </summary>
        /// <returns>Dictionary with scaled attributes</returns>
        public IDictionary<Attributes, int> GetBonuses()
        {
            IDictionary<Attributes, int> scaledAttributes = new Dictionary<Attributes, int>();

            foreach (KeyValuePair<Attributes, int> bonus in Bonus)
            {
                double scaledBonus = CalculateAttributes(bonus); // Returns scaled bonus
                scaledAttributes.Add(bonus.Key, (int)scaledBonus);
            }
            return scaledAttributes;
        }

        /// <summary> Calculates bonus based on initial bonus, bonusscaling, level and slotScaling </summary>
        double CalculateAttributes(KeyValuePair<Attributes, int> bonus)
        {
            double scaledBonus = bonus.Value // Initial bonus
                + BonusScaling[bonus.Key] * Level // BonusScaling * Level
                + GetSlotScaling(); // SlotScaling

            return scaledBonus;
        }

        /// <summary> Calculates protection stats based on which part of body item sits on</summary>
        double GetSlotScaling()
        {
            switch (ItemSlot)
            {
                case ItemSlots.BODY: return 1.0; // 100% Scaling
                case ItemSlots.HEAD: return 0.8; // 80% Scaling
                case ItemSlots.LEGS: return 0.6; // 60% Scaling
                default: return 1.0; // Default 100% scaling
            }
        }
    }
}
