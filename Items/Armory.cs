﻿using System;
using RPGCharacters.Enums;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Contains generator for random armor and weapon names
    /// </summary>
    static class Armory
    {
        static string[] greatEndings =
        {
            " of the Lone Wolf",
            " of the Exiled",
            " of the Dark Magister",
            " of the Juggernaut",
            " of the White Magister",
            " of the Warrior",
            " of the Assassin",
            " of the Giant",
            " of the Marauder",
            " of the Trickster",
            " of the Gryffin",
            " of the Lions Cub",
            " of the Knowledgable",
            " of the Brave",
            " of the Coward",
            " of the King"
        };

        static string[] meleeWeapons =
        {
            "Great Axe",
            "Great Sword",
            "Great Spear",
            "Great Dagger",
            "Great Mace",
            "LightSaber"
        };

        static string[] rangedWeapons =
        {
            "Long Bow",
            "Great AK-47",
            "Short Bow",
            "Tiny Bow",
            "Miniature Bow",
            "Bow",
            "RainBow"
        };

        static string[] magicWeapons =
        {
            "Sturdy Wand",
            "Magic Mike",
            "Staff",
            "Cursed Dagger",
            "Fire Rune",
            "Water Rune",
            "Teleportation scroll",
            "Grimoire",
            "Blessing",
            "Curse",
            "Laser Pen"
        };

        /// <summary>
        /// Returns random weapon name, based on type of weapon
        /// </summary>
        public static string GetRandomWeapon(WeaponTypes type)
        {
            Random randomIndex = new Random();
            string weaponName;
            switch (type)
            {
                case WeaponTypes.MELEE:
                    weaponName = meleeWeapons[randomIndex.Next(meleeWeapons.Length)];
                    break;
                case WeaponTypes.RANGED:
                    weaponName = rangedWeapons[randomIndex.Next(rangedWeapons.Length)];
                    break;
                case WeaponTypes.MAGIC:
                    weaponName = magicWeapons[randomIndex.Next(magicWeapons.Length)];
                    break;
                default:
                    weaponName = "Excalibur";
                    break;
            }
            return weaponName + GetGreatEnding();
        }

        /// <summary>
        /// Returns random armor name, based on ArmorType and ItemSlot
        /// </summary>
        public static string GetRandomArmor(ArmorTypes type, ItemSlots slot)
        {
            return GetArmorTypeString(type) + GetArmorSlotString(slot) + GetGreatEnding();
        }

        private static string GetGreatEnding()
        {
            Random randomIndex = new Random();
            return greatEndings[randomIndex.Next(greatEndings.Length)];
        }

        private static string GetArmorTypeString(ArmorTypes type)
        {
            switch (type)
            {
                case ArmorTypes.CLOTH:
                    return "Cloth ";
                case ArmorTypes.LEATHER:
                    return "Leather ";
                case ArmorTypes.PLATE:
                    return "Plate ";
                default:
                    return "Bikini ";
            }
        }

        private static string GetArmorSlotString(ItemSlots slot)
        {
            switch (slot)
            {
                case ItemSlots.BODY:
                    return "Chest piece";
                case ItemSlots.HEAD:
                    return "Helmet";
                case ItemSlots.LEGS:
                    return "Longjohns";
                default:
                    return "Bag";
            }
        }
    }
}
