﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Holds all base attributes for all ArmorTypes
    /// </summary>
    static class ArmorFactory
    {
        /// <summary>
        /// Generates random armor based on ItemSlot and ArmorType
        /// </summary>
        public static Armor GetArmor(ItemSlots slot, ArmorTypes armorType)
        {
            Armor armor = new Armor();
            armor.ArmorType = armorType;
            armor.Name = Armory.GetRandomArmor(armorType, slot);
            armor.Bonus = new Dictionary<Attributes, int>();
            armor.BonusScaling = new Dictionary<Attributes, int>();
            switch (armorType)
            {
                case ArmorTypes.CLOTH:
                    armor.Bonus.Add(Attributes.HP, 10);
                    armor.Bonus.Add(Attributes.INT, 3);
                    armor.Bonus.Add(Attributes.DEX, 1);

                    armor.BonusScaling.Add(Attributes.HP, 5);
                    armor.BonusScaling.Add(Attributes.INT, 2);
                    armor.BonusScaling.Add(Attributes.DEX, 1);
                    break;

                case ArmorTypes.LEATHER:
                    armor.Bonus.Add(Attributes.HP, 20);
                    armor.Bonus.Add(Attributes.STR, 1);
                    armor.Bonus.Add(Attributes.DEX, 3);

                    armor.BonusScaling.Add(Attributes.HP, 8);
                    armor.BonusScaling.Add(Attributes.STR, 1);
                    armor.BonusScaling.Add(Attributes.DEX, 2);
                    break;
                case ArmorTypes.PLATE:
                    armor.Bonus.Add(Attributes.HP, 30);
                    armor.Bonus.Add(Attributes.STR, 3);
                    armor.Bonus.Add(Attributes.DEX, 1);

                    armor.BonusScaling.Add(Attributes.HP, 12);
                    armor.BonusScaling.Add(Attributes.STR, 2);
                    armor.BonusScaling.Add(Attributes.DEX, 1);
                    break;
            }
            return armor;
        }
    }
}
