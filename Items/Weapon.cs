﻿using RPGCharacters.Enums;

namespace RPGCharacters.Items
{
    class Weapon : Item
    {
        public WeaponTypes WeaponType { get; set; }
        public int BaseDamage { get; set; }
        public int DamageScaling { get; set; }

        public Weapon() : base()
        {
            ItemSlot = ItemSlots.WEAPON;
        }
        public int GetDamage()
        {
            return BaseDamage + DamageScaling * Level;
        }
    }
}
