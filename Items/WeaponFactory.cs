﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Holds all base attributes for all WeaponTypes
    /// </summary>
    static class WeaponFactory
    {
        /// <summary>
        /// Generates random weapon based on WeaponType
        /// </summary>
        public static Weapon GetWeapon(WeaponTypes type)
        {
            Weapon weapon = new Weapon();
            weapon.WeaponType = type;
            weapon.Name = Armory.GetRandomWeapon(type);
            switch (type)
            {
                case WeaponTypes.MELEE:
                    weapon.BaseDamage = 15;
                    weapon.DamageScaling = 2;
                    break;

                case WeaponTypes.MAGIC:
                    weapon.BaseDamage = 25;
                    weapon.DamageScaling = 2;
                    break;
                case WeaponTypes.RANGED:
                    weapon.BaseDamage = 5;
                    weapon.DamageScaling = 3;
                    break;
                default:
                    weapon.BaseDamage = 9999;
                    weapon.DamageScaling = 9999;
                    break;
            }
            return weapon;
        }
    }
}
