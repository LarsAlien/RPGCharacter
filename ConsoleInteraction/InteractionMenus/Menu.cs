﻿using RPGCharacters.ConsoleInteraction.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.ConsoleInteraction.InteractionMenus
{
    /// <summary>
    /// Takes in a list of options, prints them to console and returns input from user
    /// </summary>
    public class Menu
    {
        public static IDictionary<int, string> Options { get; set; }
        public Menu(IDictionary<int, string> options)
        {
            Options = options;
        }


        public int StartMenu()
        {
            foreach (KeyValuePair<int, string> entry in Options)
            {
                Console.WriteLine(entry.Key + " : " + entry.Value);
            }
            Console.WriteLine();
            Console.WriteLine("Type your option and hit Enter:");
            int inputKey = InputService.ConsoleReadInt();

            return inputKey;
        }


    }
}
