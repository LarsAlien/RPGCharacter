﻿using RPGCharacters.Adventurers;
using RPGCharacters.Enums;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.ConsoleInteraction.InteractionMenus
{
    static class ItemMenus
    {

        /// <summary>
        /// Prompts which kind of weapon the user want the adventurer to equip!
        /// </summary>
        public static void EquipWeaponMenu(Adventurer adventurer)
        {

            IDictionary<int, string> options = new Dictionary<int, string>()
            {
                {1, "Equip melee weapon"},
                {2, "Equip ranged weapon!!"},
                {3, "Equip magic weapon!!"},
                {4, "Return to your adventurer" }
            };
            Menu menu = new Menu(options);

            Console.WriteLine("Weapon menu");
            Console.WriteLine();

            int inputKey = menu.StartMenu();

            switch (inputKey)
            {
                case 1:
                    adventurer.EquipWeapon(WeaponFactory.GetWeapon(Enums.WeaponTypes.MELEE));
                    break;
                case 2:
                    adventurer.EquipWeapon(WeaponFactory.GetWeapon(Enums.WeaponTypes.RANGED));
                    break;
                case 3:
                    adventurer.EquipWeapon(WeaponFactory.GetWeapon(Enums.WeaponTypes.MAGIC));
                    break;

                default:
                    Console.Clear();
                    AdventurerMenus.CharacterMenu(adventurer);
                    break;
            }
            Console.Clear();
            AdventurerMenus.CharacterMenu(adventurer);
        }


        /// <summary>
        /// Prompts which type of armor the user want and which ItemSlot it should be inserted!
        /// </summary>
        public static Adventurer EquipArmorMenu(Adventurer adventurer)
        {
            ArmorTypes armorType = ChooseArmorTypeMenu();
            ItemSlots itemSlot = ChooseItemSlotMenu();
            adventurer.EquipArmor(itemSlot, ArmorFactory.GetArmor(itemSlot, armorType));
            return adventurer;
        }
        /// <summary>
        /// Prompts which type of armor the user want
        /// </summary>
        public static ArmorTypes ChooseArmorTypeMenu()
        {
            IDictionary<int, string> options = new Dictionary<int, string>();

            foreach (ArmorTypes aType in Enum.GetValues(typeof(ArmorTypes)))
            {
                options.Add(options.Count + 1, aType.ToString());
            }
            options.Add(options.Count + 1, "Return to your adventurer");


            Menu menu = new Menu(options);

            Console.WriteLine();
            Console.WriteLine("Choose type of armor:");
            int inputKey = menu.StartMenu();

            // Gets the armortype based on which option was chosen.
            ArmorTypes armorType = (ArmorTypes)Enum.GetValues(typeof(ArmorTypes)).GetValue(inputKey - 1);
            Console.WriteLine($"You chose {armorType} armor");
            return armorType;
        }
        /// <summary>
        /// Prompts in which ItemSlot the item should be inserted!
        /// </summary>
        public static ItemSlots ChooseItemSlotMenu()
        {
            IDictionary<int, string> options = new Dictionary<int, string>()
            {
                {1, "Body"},
                {2, "Head"},
                {3, "Legs"},
                {4, "Return to your adventurer" }
            };

            Menu menu = new Menu(options);

            Console.WriteLine();
            Console.WriteLine("Choose slot of armor:");

            int inputKey = menu.StartMenu();

            ItemSlots itemSlot = (ItemSlots)Enum.GetValues(typeof(ItemSlots)).GetValue(inputKey);
            Console.WriteLine($"You chose {itemSlot} armor");
            return itemSlot;
        }
    }
}
