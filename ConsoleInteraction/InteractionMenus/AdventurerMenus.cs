﻿using RPGCharacters.Adventurers;
using RPGCharacters.ConsoleInteraction.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.ConsoleInteraction.InteractionMenus
{
    static class AdventurerMenus
    {
        /// <summary>
        /// Prompts the types of adventurers the user can create
        /// </summary>
        public static void CharacterCreationMenu()
        {
            Console.Clear();
            IDictionary<int, string> options = new Dictionary<int, string>();

            foreach (Enums.AdventurerClasses aClass in Enum.GetValues(typeof(Enums.AdventurerClasses)))
            {
                options.Add(options.Count + 1, $"Create new {aClass}");
            }
            options.Add(options.Count + 1, "Return to main menu");

            Menu menu = new Menu(options);

            Console.WriteLine();


            int inputKey = menu.StartMenu();
            Adventurer adventurer;
            switch (inputKey)
            {
                case 1:
                    adventurer = new Adventurer(Enums.AdventurerClasses.BRUTE);
                    break;
                case 2:
                    adventurer = new Adventurer(Enums.AdventurerClasses.BOWMASTER);
                    break;
                case 3:
                    adventurer = new Adventurer(Enums.AdventurerClasses.MAGISTER);
                    break;
                default:
                    adventurer = new Adventurer(Enums.AdventurerClasses.BRUTE);
                    break;
            }
            AdventurerFactory.AddAdventurer(adventurer);
            Console.Clear();
            CharacterMenu(adventurer);
        }

        /// <summary>
        /// Shows all created adventurers, returns CharacterCreationMenu if none are found
        /// </summary>
        public static void YourCharactersMenu()
        {
            IDictionary<int, string> options = new Dictionary<int, string>();
            List<Adventurer> adventurers = AdventurerFactory.GetAdventurers();
            if (adventurers.Count == 0)
            {
                Console.WriteLine("You have no characters yet, Create a new one!");
                CharacterCreationMenu();
            }
            else
            {
                Console.WriteLine("Your characters(Choose character to display actions!):");
                Console.WriteLine();
                foreach (Adventurer adventurer in adventurers)
                {
                    options.Add(options.Count + 1, adventurer.Name);
                }
                options.Add(options.Count + 1, "Return to Main menu");
                Menu menu = new Menu(options);

                Console.WriteLine();

                int inputKey = menu.StartMenu();
                Console.Clear();
                CharacterMenu(AdventurerFactory.GetAdventurers()[inputKey - 1]);
            }

        }

        /// <summary>
        /// Shows stats and actions for chosen adventurer
        /// </summary>
        public static void CharacterMenu(Adventurer adventurer)
        {
            PrintingService.PrintAdventurerStats(adventurer);

            IDictionary<int, string> options = new Dictionary<int, string>()
            {
                {1, "Attack!!"},
                {2, adventurer.weapon is null ? "Equip weapon!!" : "Unequip weapon!!"},
                {3, "Equip armor!!"},
                {4, "Return to main menu" }
            };
            Menu menu = new Menu(options);

            Console.WriteLine();

            int inputKey = menu.StartMenu();

            switch (inputKey)
            {
                case 1:
                    Console.Clear();
                    adventurer.Attack();
                    CharacterMenu(adventurer);
                    break;
                case 2:
                    Console.Clear();
                    if (adventurer.weapon is null) ItemMenus.EquipWeaponMenu(adventurer); else adventurer.UnequipWeapon();
                    CharacterMenu(adventurer);
                    break;
                case 3:
                    Console.Clear();
                    adventurer = ItemMenus.EquipArmorMenu(adventurer);
                    Console.Clear();
                    CharacterMenu(adventurer);
                    break;
                case 4:
                    Console.Clear();
                    MainMenus.MainMenu();
                    break;

                default:
                    Console.Clear();
                    CharacterMenu(adventurer);
                    break;
            }
        }
    }
}
