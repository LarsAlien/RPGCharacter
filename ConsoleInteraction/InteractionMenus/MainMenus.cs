﻿using System;
using System.Collections.Generic;

namespace RPGCharacters.ConsoleInteraction.InteractionMenus
{
    static class MainMenus
    {
        /// <summary>
        /// Shows the main menu, and prompts which action to take
        /// </summary>
        public static void MainMenu()
        {
            IDictionary<int, string> options = new Dictionary<int, string>()
            {
                {1, "Character creation!"},
                {2, "Your characters"},
                {3, "Exit application"},
            };
            Menu mainMenu = new Menu(options);
            Console.WriteLine("Welcome to the RPGCharacter manager!");
            Console.WriteLine();

            int inputKey = mainMenu.StartMenu();

            switch (inputKey)
            {
                case 1:
                    AdventurerMenus.CharacterCreationMenu();
                    break;
                case 2:
                    AdventurerMenus.YourCharactersMenu();
                    break;
                case 3:
                    Environment.Exit(0);
                    break;

                default:
                    MainMenu();
                    break;
            }
        }
    }
}
