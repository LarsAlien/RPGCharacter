﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.ConsoleInteraction.Services
{
    /// <summary>
    /// Requests input from user
    /// </summary>
    static class InputService
    {
        /// <summary>
        /// Requests number from user
        /// </summary>
        public static int ConsoleReadInt()
        {
            Console.WriteLine();
            string readInt = Console.ReadLine();
            return int.TryParse(readInt, out int j)
                    ? j
                    : ConsoleReadInt();
        }

        /// <summary>
        /// Requests string from user
        /// </summary>
        public static string ConsoleReadString()
        {
            Console.WriteLine();
            string readString = Console.ReadLine();
            return readString is string
                    ? readString
                    : ConsoleReadString();
        }
    }
}
