﻿using System;
using System.Collections.Generic;
using System.Text;
using RPGCharacters.Adventurers;
using RPGCharacters.Enums;
using RPGCharacters.Items;

namespace RPGCharacters.ConsoleInteraction.Services
{
    /// <summary>
    /// PrintingService for printing readable objects to console
    /// </summary>
    static class PrintingService
    {
        public static void PrintAdventurerStats(Adventurer adventurer)
        {
            Console.WriteLine($"Adventurer: {adventurer.Name} (lvl:{adventurer.Level})");
            Console.WriteLine($"Health: {adventurer.Health}");
            Console.WriteLine($"Strength: {adventurer.Strength}");
            Console.WriteLine($"Dexterity: {adventurer.Dexterity}");
            Console.WriteLine($"Intelligence: {adventurer.Intelligence}");

            if (adventurer.armor is { })
            {
                foreach (KeyValuePair<ItemSlots, Armor> armor in adventurer.armor)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Equipped {armor.Key} armor: {armor.Value.Name} (lvl:{armor.Value.Level})");
                }
            }

            if (adventurer.weapon is { })
            {
                Console.WriteLine();
                Console.WriteLine($"Equipped weapon: {adventurer.weapon.Name} (lvl:{adventurer.weapon.Level})");
            }
        }
    }
}
